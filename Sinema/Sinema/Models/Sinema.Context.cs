﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public class SinemaDBEntities : DbContext
    {
        public SinemaDBEntities()
            : base("name=SinemaDBEntities")
        {           
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
        public virtual DbSet<Filmler> Filmler { get; set; }
        public virtual DbSet<Zaman> Zaman { get; set; }
        public virtual DbSet<Biletler> Biletler { get; set; }
        public virtual DbSet<SeansKayit> SeansKayit { get; set; }
        public virtual DbSet<GösterimYeri> GösterimYeri { get; set; }
        public virtual DbSet<Kayit> Kayit { get; set; }
        public virtual DbSet<Slider> Slider { get; set; }
   

    }
}