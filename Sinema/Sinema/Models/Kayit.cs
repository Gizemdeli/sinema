﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Collections.Generic;
    public partial class Kayit
    {
        public int KayitID { get; set; }
        public Nullable<int> FilmID { get; set; }
        public Nullable<int> YerID { get; set; }


        public virtual Filmler Filmler { get; set; }
        public virtual GösterimYeri GösterimYeri { get; set; }
    }
}