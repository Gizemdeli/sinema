﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Collections.Generic;
    public partial class Slider
    {
        public int ID { get; set; }
        public byte[] SliderFoto { get; set; }
        public string SliderText { get; set; }
        public Nullable<System.DateTime> BaslangicTarih { get; set; }
        public Nullable<System.DateTime> BitisTarih { get; set; }
    }
}