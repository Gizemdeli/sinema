﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Collections.Generic;
    public class Biletler
    {

        public int BiletNo { get; set; }
        public Nullable<int> KoltukNo { get; set; }
        public string BiletTipi { get; set; }
        public string ÖdemeSekli { get; set; }
        public virtual Filmler Filmler { get; set; }
    }
}