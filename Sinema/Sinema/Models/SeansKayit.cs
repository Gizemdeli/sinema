﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Collections.Generic;

    public partial class SeansKayit
    {
        public int SeansKayit1 { get; set; }
        public Nullable<int> FilmID { get; set; }
        public Nullable<int> ZamanID { get; set; }
      

        public virtual Filmler Filmler { get; set; }
        public virtual Zaman Zaman { get; set; }
    }
}