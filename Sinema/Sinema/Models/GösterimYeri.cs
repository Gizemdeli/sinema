﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Collections.Generic;
    public class GösterimYeri
    {
        public GösterimYeri()
        {
            this.Kayit = new HashSet<Kayit>();
        }

        public int YerID { get; set; }
        public string GösterildiğiSinemaAdi{ get; set; }
        public Nullable<int> GösterildiğiSalonNo { get; set; }
        public string İl { get; set; }
        public string İlçe { get; set; }
        public virtual ICollection<Kayit> Kayit { get; set; }
    }
}