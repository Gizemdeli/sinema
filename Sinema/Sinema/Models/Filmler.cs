﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Collections.Generic;

    public partial class Filmler
    {
        public Filmler()
        {
            this.SeansKayit = new HashSet<SeansKayit>();
            this.Kayit = new HashSet<Kayit>();
        }
        public int FilmID { get; set; }
        public Nullable<int> BiletNo { get; set; }
        public string FilmAdi { get; set; }
        public string FilmTürü { get; set; }
        public Nullable<System.DateTime> VizyonTarihi { get; set; }
        public Nullable<int> FilmSüresi { get; set; }
        public string FilmYönetmeni { get; set; }
        public string FilmOyuncuları { get; set; }
        public string VizyondaOlanFilmler { get; set; }
        public string GelecekFilmler { get; set; }


        public virtual Biletler Biletler { get; set; }
        public virtual ICollection<SeansKayit> SeansKayit { get; set; }
        public virtual ICollection<Kayit> Kayit { get; set; }
    }
}