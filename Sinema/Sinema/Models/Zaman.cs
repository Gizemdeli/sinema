﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinema.Models
{
    using System;
    using System.Collections.Generic;
    public partial class Zaman
    {
        public Zaman()
        {
            this.SeansKayit = new HashSet<SeansKayit>();
        }

        public int ZamanID { get; set; }
        public Nullable<System.DateTime> SeansTarihi { get; set; }
        public Nullable<int> SeansSaati { get; set; }
        public virtual ICollection<SeansKayit> SeansKayit { get; set; }
    }
}