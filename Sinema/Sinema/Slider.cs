//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sinema
{
    using System;
    using System.Collections.Generic;
    
    public partial class Slider
    {
        public int ID { get; set; }
        public byte[] SliderFoto { get; set; }
        public string SliderText { get; set; }
        public Nullable<System.DateTime> BaslangıcTarih { get; set; }
        public Nullable<System.DateTime> BitişTarih { get; set; }
    }
}
