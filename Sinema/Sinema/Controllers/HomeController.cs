﻿using Sinema.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Sinema.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "BİLET İŞLEMLERİ İÇİN ;";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "İLETİŞİM İÇİN";

            return View();
        }
        public ActionResult ChangeCulture(string Home, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(Home);
            return Redirect(returnUrl);
        }


    }
}