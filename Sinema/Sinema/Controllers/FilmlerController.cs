﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace Sinema.Controllers
{
    public class FilmlerController : Controller
    {
     private SinemaDBEntities db = new SinemaDBEntities();

        // GET: Filmler
        public ActionResult Index()
        {
            var filmler = db.Filmler.Include(f => f.Biletler);
            return View(filmler.ToList());
        }

        // GET: Filmler/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filmler filmler = db .Filmler.Find(id);
            if (filmler == null)
            {
                return HttpNotFound();
            }
            return View(filmler);
        }

        // GET: Filmler/Create
        public ActionResult Create()
        {
            ViewBag.BiletNo = new SelectList(db.Biletler, "BiletNo", "BiletTipi");
            return View();
        }

        // POST: Filmler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FilmID,FilmAdi,FilmTürü,FilmSüresi,VizyonTarihi,FilmYönetmeni,FilmOyuncuları,VizyondaOlanFilmler,GelecekFilmler,BiletTipi")] Filmler filmler)
        {
            if (ModelState.IsValid)
            {
                db.Filmler.Add(filmler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BiletNo = new SelectList(db.Biletler, "BiletNo", "BiletTipi", filmler.BiletNo);
            return View(filmler);
        }

        // GET: Filmler/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filmler filmler = db.Filmler.Find(id);
            if (filmler == null)
            {
                return HttpNotFound();
            }
            ViewBag.BiletNo = new SelectList(db.Biletler, "BiletNo", "BiletTipi", filmler.BiletNo);
            return View(filmler);
        }

        // POST: Filmler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FilmID,FilmAdi,FilmTürü,FilmSüresi,VizyonTarihi,FilmYönetmeni,FilmOyuncuları,VizyondaOlanFilmler,GelecekFilmler,BiletNo")] Filmler filmler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(filmler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BiletNo = new SelectList(db.Biletler, "BiletNo", "BiletTipi", filmler.BiletNo);
            return View(filmler);
        }

        // GET: Filmler/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filmler filmler = db.Filmler.Find(id);
            if (filmler == null)
            {
                return HttpNotFound();
            }
            return View(filmler);
        }

        // POST: Filmler/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Filmler filmler = db.Filmler.Find(id);
            db.Filmler.Remove(filmler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
